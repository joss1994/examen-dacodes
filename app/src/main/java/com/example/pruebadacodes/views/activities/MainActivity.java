package com.example.pruebadacodes.views.activities;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pruebadacodes.R;
import com.example.pruebadacodes.callbacks.MainCallBack;
import com.example.pruebadacodes.callbacks.MainPresenter;
import com.example.pruebadacodes.models.Drinks;
import com.example.pruebadacodes.views.adapters.DrinksAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener, MainCallBack.view {

    @BindView(R.id.rvDrinks)
    RecyclerView rvDrinks;
    @BindView(R.id.contentNotFound)
    RelativeLayout contentNotFound;

    private MainPresenter mPresenter;
    private DrinksAdapter mAdapter;
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPresenter = new MainPresenter(this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.OnGetDrinks();
    }

    @Override
    public void OnSuccessGetDrinks(ArrayList<Drinks> drinks) {
        if (mAdapter == null){
            mAdapter = new DrinksAdapter(drinks, this);
            rvDrinks.setLayoutManager(new LinearLayoutManager(this));
            rvDrinks.setHasFixedSize(true);
            rvDrinks.setAdapter(mAdapter);
        }else{
            mAdapter.updateData(drinks);
        }
    }

    @Override
    public void OnErrorGetDrinks(String message) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        mSearchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();
        setupSearchView();

        int closeButtonId = getResources().getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButtonImage = mSearchView.findViewById(closeButtonId);
        closeButtonImage.setImageResource(R.drawable.ic_baseline_close_24);
        return true;
    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(true);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) { List<SearchableInfo> searchables = searchManager.getSearchablesInGlobalSearch();

            SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
            for (SearchableInfo inf : searchables) {
                if (inf.getSuggestAuthority() != null && inf.getSuggestAuthority().startsWith("applications")) {
                    info = inf;
                }
            }
            mSearchView.setSearchableInfo(info);
        }

        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
    }

    public boolean onQueryTextChange(String newText) {
        mAdapter.getFilter().filter(newText);
        if (newText.isEmpty()){
            rvDrinks.setVisibility(View.VISIBLE);
            contentNotFound.setVisibility(View.GONE);
        }else {
            if (mAdapter.getItemCount() < 1) {
                rvDrinks.setVisibility(View.GONE);
                contentNotFound.setVisibility(View.VISIBLE);
            } else {
                rvDrinks.setVisibility(View.VISIBLE);
                contentNotFound.setVisibility(View.GONE);
            }
        }
        return false;
    }

    public boolean onQueryTextSubmit(String query) {
        mAdapter.getFilter().filter(query);
        return false;
    }

    public boolean onClose() {
        return false;
    }
}