package com.example.pruebadacodes.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pruebadacodes.R;
import com.example.pruebadacodes.models.Drinks;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DrinksAdapter extends RecyclerView.Adapter<DrinksAdapter.DrinksAdapterViewHolder> implements Filterable {

    private ArrayList<Drinks> listDrinks;
    private ArrayList<Drinks> listDrinksAux;
    private Context mContext;

    public DrinksAdapter(ArrayList<Drinks> listDrinks, Context mContext) {
        this.listDrinks = listDrinks;
        this.listDrinksAux = listDrinks;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public DrinksAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_drink, parent, false);
        return new DrinksAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DrinksAdapterViewHolder holder, int position) {
        Drinks data = listDrinksAux.get(position);
        holder.txtNameDribnks.setText(data.getStrDrink());
        holder.txtDescriptionDrink.setText(data.getStrInstructions());
        Picasso.with(mContext).load(data.getStrDrinkThumb()).into(holder.drinks_image);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listDrinksAux == null ? 0 : listDrinksAux.size();

    }

    public void updateData(ArrayList<Drinks> listDrinks){
        this.listDrinks = listDrinks;
        this.listDrinksAux = listDrinks;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listDrinksAux = listDrinks;
                } else {
                    ArrayList<Drinks> filteredList = new ArrayList<>();
                    for (Drinks row : listDrinks) {

                        if (row.getStrDrink().toLowerCase().startsWith(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    listDrinksAux = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listDrinksAux;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listDrinksAux = (ArrayList<Drinks>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    static class DrinksAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.drinks_image)
        CircleImageView drinks_image;
        @BindView(R.id.txtNameDribnks)
        TextView txtNameDribnks;
        @BindView(R.id.txtDescriptionDrink)
        TextView txtDescriptionDrink;
        @BindView(R.id.contentDrink)
        CardView contentDrink;

        public DrinksAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}


