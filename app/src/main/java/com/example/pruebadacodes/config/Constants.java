package com.example.pruebadacodes.config;

public  class Constants {

    public static final String BASE_URL = "https://www.thecocktaildb.com/api/";
    public static final String URL_DRINKS = BASE_URL + "json/v1/1/search.php?s=margarita";
}
