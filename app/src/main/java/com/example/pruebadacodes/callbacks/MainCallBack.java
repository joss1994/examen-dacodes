package com.example.pruebadacodes.callbacks;

import com.example.pruebadacodes.models.Drinks;

import java.util.ArrayList;

public interface MainCallBack {

    interface view {
        void OnSuccessGetDrinks(ArrayList<Drinks> drinks);

        void OnErrorGetDrinks(String message);
    }

    interface presenter {
        void OnGetDrinks();
    }
}
