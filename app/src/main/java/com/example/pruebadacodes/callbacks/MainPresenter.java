package com.example.pruebadacodes.callbacks;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.pruebadacodes.R;
import com.example.pruebadacodes.config.Constants;
import com.example.pruebadacodes.models.Drinks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainPresenter implements MainCallBack.presenter {

    private Context mContext;
    private MainCallBack.view mView;

    public MainPresenter(Context mContext, MainCallBack.view mView) {
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void OnGetDrinks() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.URL_DRINKS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ArrayList<Drinks> myDrinks = new ArrayList<>();
                if (!response.equals("[]")) {
                    try {
                        JSONObject obj = new JSONObject(response);
                        JSONArray drinksArray = obj.getJSONArray("drinks");
                        for (int i = 0; i < drinksArray.length(); i++) {
                            JSONObject heroObject = drinksArray.getJSONObject(i);
                            Drinks drink = new Drinks(
                                    heroObject.getString("idDrink"),
                                    heroObject.getString("strDrink"),
                                    heroObject.getString("strInstructions"),
                                    heroObject.getString("strDrinkThumb"));
                            myDrinks.add(drink);
                        }

                        mView.OnSuccessGetDrinks(myDrinks);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mView.OnErrorGetDrinks(mContext.getString(R.string.error_get_drinks));
                    }
                }else{
                    mView.OnErrorGetDrinks(mContext.getString(R.string.empty_fields));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mView.OnErrorGetDrinks(mContext.getString(R.string.error_get_drinks));
            }
        });

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 30000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 30000;
            }

            @Override
            public void retry(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);

    }


}
